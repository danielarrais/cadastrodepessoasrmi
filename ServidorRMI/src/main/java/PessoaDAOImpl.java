import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PessoaDAOImpl implements PessoaDao{

    public void inserir(Pessoa pessoa) {
        String sql = "INSERT INTO pessoas (nome, sobrenome) VALUES (?, ?)";
        Connection connection = new ConnectionFactory().getConnection(ConnectionType.INSERT_DELETE_UPDATE);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, pessoa.getNome());
            statement.setString(2, pessoa.getSobrenome());

            statement.execute();
            statement.close();

            System.out.println("Pessoa inserida!");
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void deletar(int id) {
        String sql = "DELETE FROM pessoas WHERE id = ?";
        Connection connection = new ConnectionFactory().getConnection(ConnectionType.INSERT_DELETE_UPDATE);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            int r = statement.executeUpdate();
            statement.close();

            if (r > 0) {
                System.out.println("Pessoa deletada!");
            } else {
                System.out.println("Erro ao deletar pessoa!");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void alterar(Pessoa pessoa) {
        String sql = "UPDATE pessoas SET nome = ?, sobrenome = ? WHERE id = ?";
        Connection connection = new ConnectionFactory().getConnection(ConnectionType.INSERT_DELETE_UPDATE);

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setString(1, pessoa.getNome());
            statement.setString(2, pessoa.getSobrenome());
            statement.setInt(3, pessoa.getId());

            int a = statement.executeUpdate();
            statement.close();

            if (a > 0) {
                System.out.println("Pessoa alterada!");
            } else {
                System.out.println("Erro ao alterar pessoa!");
            }
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public Pessoa buscar(int id) {
        String sql = "SELECT * FROM pessoas WHERE id = ?";
        Connection connection = new ConnectionFactory().getConnection(ConnectionType.SELECT);
        Pessoa pessoa = null;

        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, id);

            ResultSet rs = statement.executeQuery();

            if (rs.next()) {
                pessoa = new Pessoa();

                pessoa.setId(rs.getInt("id"));
                pessoa.setNome(rs.getString("nome"));
                pessoa.setSobrenome(rs.getString("sobrenome"));
            }

            statement.close();
        } catch (SQLException ex) {
            Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(PessoaDAOImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return pessoa;
    }
}

    
