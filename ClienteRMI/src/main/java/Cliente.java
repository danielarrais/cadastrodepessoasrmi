import com.sun.xml.internal.ws.util.StringUtils;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cliente {

    private Scanner scanner;
    private Registry registry;
    private PessoaDao stub;

    public Cliente() {
        scanner = new Scanner(System.in);
        String ip;
        try {
            System.out.print("Informe o IP do servidor (0->localhost): ");
            ip = scanner.next();
            registry = LocateRegistry.getRegistry(ip == null || ip.equals("0") ? "localhost" : ip, 1099);
            stub = (PessoaDao) registry.lookup("persistencia_pessoas");
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void exibirOpcoes() {
        int opcao = 0;
        while (true) {
            System.out.println("Escolha a ação que você deseja realizar:");
            System.out.println();
            System.out.println("Inserir.......[1]");
            System.out.println("Deletar.......[2]");
            System.out.println("Alterar.......[3]");
            System.out.println("Buscar........[4]");
            System.out.println("Sair..........[0]\n");

            System.out.print("Alternativa: ");
            opcao = scanner.nextInt();
            System.out.println();
            switch (opcao) {
                case 0:
                    System.exit(0);
                case 1:
                    inserir();
                    break;
                case 2:
                    deletar();
                    break;
                case 3:
                    alterar();
                    break;
                case 4:
                    buscar();
                    break;        
                default:
                    System.out.println("Erro!!! Opção inválida!\n");
            }
            System.out.println();
        }
    }
    private void inserir() {
        Pessoa pessoa = new Pessoa();
        scanner.nextLine();

        System.out.println("Informe os dados: ");
        System.out.print("Nome: ");
        pessoa.setNome(scanner.nextLine());

        System.out.print("Sobrenome: ");
        pessoa.setSobrenome(scanner.nextLine());

        try {
            stub.inserir(pessoa);
        } catch (RemoteException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        private void buscar() {
        System.out.print("Id: ");
        int id = scanner.nextInt();

        try {
            Pessoa pessoa = stub.buscar(id);

            if (pessoa != null) {
                System.out.println();
                System.out.println("RESULTADO:");
                System.out.println();
                System.out.println("ID: " + pessoa.getId());
                System.out.println("Nome: " + pessoa.getNome());
                System.out.println("Sobrenome.: " + pessoa.getSobrenome());
            } else {
                System.out.println("O ID " + id + "da Pessoa "+pessoa+" não localizado!");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void deletar() {
        System.out.print("Id: ");
        int id = scanner.nextInt();

        try {
            stub.deletar(id);
        } catch (RemoteException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void alterar() {
        System.out.print("Id: ");
        int id = scanner.nextInt();

        try {
            Pessoa pessoa = stub.buscar(id);

            if (pessoa != null) {
                scanner.nextLine();

                System.out.print("Nome: ");
                pessoa.setNome(scanner.nextLine());

                System.out.print("Sobrenome: ");
                pessoa.setSobrenome(scanner.next());

                stub.alterar(pessoa);
            } else {
                System.out.println("O ID " + id + "da Pessora "+pessoa+" não localizado!");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
